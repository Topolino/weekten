# Name: Joey Carberry
# Date: November 10, 2015
# Project: Project Nine - Dictionaries, and encryption
file = open('test.txt', 'r')
fileText = list(file.read())
fileTextLen = len(fileText)
encryptedFile = open('encrypted.txt', 'a')
encryptionDictionary = {'A': '!', 'B': '@', 'C': '#', 'D': '$', 'E': '%', 'F': '^', 'G': '&', 'H': '*', 'I': '(',
                        'J': ')', 'K': '-', 'L': '_', 'M': '+', 'N': '=', 'O': '{', 'P': '[', 'Q': '}', 'R': '}',
                        'S': ']', 'T': '|', 'U': ':', 'V': ';', 'W': '"', 'X': '<', 'Y': ',', 'Z': '>', ' ': '.',
                        '.': '?', '?': '~', ',': '`', '0': 'Q', '1': 'W', '2': 'E', '3': 'R', '4': 'T', '5': 'Y',
                        '6': 'U', '7': 'I', '8': 'O', '9': 'P', 'a': '9', 'b': '8', 'c': '7', 'd': '6', 'e': '5',
                        'f': '4', 'g': '3', 'h': '2', 'i': '1', 'j': '0', 'k': 'A', 'l': 'B', 'm': 'C', 'n': 'D',
                        'o': 'Z', 'p': 'F', 'q': 'G', 'r': 'H', 's': 'X', 't': 'J', 'u': 'K', 'v': 'L', 'w': 'M',
                        'x': 'N', 'y': 'Y', 'z': 'P', '-': 'S'}

for x in range(0, fileTextLen):
    if fileText[x] == '\n':
        encryptedFile.write('\n')

    if fileText[x] in encryptionDictionary:
        encryptedText = encryptionDictionary.get(fileText[x])
        encryptedFile.write(encryptedText)
