# Name: Joey Carberry
# Date: November 16, 2015
# Project: Project 9 unique words

def uniqueFile(inputFilename, outputFilename):
    nC = 0
    inputFile = open(inputFilename, 'r')
    fileContents = inputFile.read()
    inputFile.close()
    wordList = fileContents.split()

    file = open(outputFilename, 'w')

    uniqueWords = set(wordList)
    for word in uniqueWords:
        file.write(str(word) + " ")
        nC += 1
        if nC == 16:
            file.write('\n')
            nC = 0

uniqueFile('test.txt', 'uniqueWords.txt')
