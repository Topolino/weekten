# Name: Joey Carberry
# Date: November 12, 2015
# Project: Project 9 - Decryption
encryptionDictionary = {'A': '!', 'B': '@', 'C': '#', 'D': '$', 'E': '%', 'F': '^', 'G': '&', 'H': '*', 'I': '(',
                        'J': ')', 'K': '-', 'L': '_', 'M': '+', 'N': '=', 'O': '{', 'P': '[', 'Q': '}', 'R': '}',
                        'S': ']', 'T': '|', 'U': ':', 'V': ';', 'W': '"', 'X': '<', 'Y': ',', 'Z': '>', ' ': '.',
                        '.': '?', '?': '~', ',': '`', '0': 'Q', '1': 'W', '2': 'E', '3': 'R', '4': 'T', '5': 'Y',
                        '6': 'U', '7': 'I', '8': 'O', '9': 'P', 'a': '9', 'b': '8', 'c': '7', 'd': '6', 'e': '5',
                        'f': '4', 'g': '3', 'h': '2', 'i': '1', 'j': '0', 'k': 'A', 'l': 'B', 'm': 'C', 'n': 'D',
                        'o': 'Z', 'p': 'F', 'q': 'G', 'r': 'H', 's': 'X', 't': 'J', 'u': 'K', 'v': 'L', 'w': 'M',
                        'x': 'N', 'y': 'Y', 'z': 'P', '-': 'S'}

# Dictionaries go, key: value

file = open('encrypted.txt', 'r')
fileText = list(file.read())
fileTextLen = len(fileText)
decryptedFile = open('decrypted.txt', 'a')
decryptionDictionary = {y: x for x, y in encryptionDictionary.items()}
for x in range(0,fileTextLen):
    if fileText[x] == '\n':
        decryptedFile.write('\n')
    if fileText[x] in decryptionDictionary:
        decryptedText = decryptionDictionary.get(fileText[x])
        decryptedFile.write(decryptedText)