# Name: Joey Carberry
# Date: November 17, 2015
# Project: Project 9 - File Analysis
userFile1 = input('What file would you like to analyze first? ')
userFile2 = input('What file would you like to analyze second? ')
userOutputFile = input('What file will be the output? (Note: It will be clobbered) ')

def uniqueFile(inputFilename1, inputFilename2, outputFilename):
    nC = 0
    inputFile1 = open(inputFilename1, 'r')
    inputFile2 = open(inputFilename2, 'r')
    fileContents1 = inputFile1.read()
    fileContents2 = inputFile2.read()
    inputFile1.close()
    inputFile2.close()
    wordList1 = fileContents1.split()
    wordList2 = fileContents2.split()

    file = open(outputFilename, 'w')

    uniqueWords1 = set(wordList1)
    for word in uniqueWords1:
        file.write(str(word) + " ")
        nC += 1
        if nC == 16:
            file.write('\n')
            nC = 0
    file.write('\n')
    file.write('\n')
    file.write(str('Second File Unique Words' + '\n'))
    file.write('\n')
    uniqueWords2 = set(wordList2)
    for word in uniqueWords2:
        file.write(str(word) + " ")
        nC += 1
        if nC == 16:
            file.write('\n')
            nC = 0
def intersectionFile(inputFileName1, inputFileName2):
    inputFile1 = open(inputFileName1, 'r')
    inputFile2 = open(inputFileName2, 'r')
    file1Set = set(inputFile1.read().split())
    file2Set = set(inputFile2.read().split())
    intersectionSet = file1Set.intersection(file2Set)
    print(intersectionSet)

def differenceFile(inputFileName1, inputFileName2):
    inputFile1 = open(inputFileName1, 'r')
    inputFile2 = open(inputFileName2, 'r')
    file1Set = set(inputFile1.read().split())
    file2Set = set(inputFile2.read().split())
    print('Words that are in the first file but not the second file: ')
    print(file1Set.difference(file2Set))
    print('Words that are in the second file but not in the first file.')
    print(file2Set.difference(file1Set))
    combinedDifferenceSet = (file1Set.difference(file2Set)) + (file2Set.difference(file1Set))
    print('Words that are in either the first or second file but not in both.')
    print(combinedDifferenceSet)

print('The unique words in both files has been outputted into ' + userOutputFile)
uniqueFile(userFile1,userFile2, userOutputFile)
print('The intersections of the files has been found: ')
intersectionFile(userFile1, userFile2)
differenceFile(userFile1, userFile2)
